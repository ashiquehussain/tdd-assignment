import {expect} from "chai";
import * as assets from "./fixtures/assetsTestData.json";

const RmpvConstants: any = {
    rmpvMatrix: {
        inkMultifunctionA3: 7500,
        inkMultifunctionA4: 4500,
        inkOtherA3: 3000,
        inkOtherA4: 3000,
        inkSingleFunctionA3: 5000,
        inkSingleFunctionA4: 3000,
        Scan: 3000,
        laserMultifunctionA3: 15000,
        laserMultifunctionA4: 7500,
        laserOtherA3: 3000,
        laserOtherA4: 3000,
        laserSingleFunctionA3: 10000,
        laserSingleFunctionA4: 5000
    },
    deviceTypeMatrix: {
        fax: 'Other',
        multifunctionColor: 'Multifunction',
        multifunctionMono: 'Multifunction',
        plotter: 'Other',
        printColor: 'SingleFunction',
        printMono: 'SingleFunction',
        scanner: 'Scanner',
        unknown: 'unknown'
    },
    defaultRmpv: 5000,
    defaultRmpvForPlotterAndFax: 3000,
    A3: "A3",
    A4: "A4",
    Scan: "Scan"
};

const Configuration: any = {
    deviceTypes: {
        fax: 'fax',
        plotter: 'plotter',
    }
};

function getRmpv(asset: any) {
    var rmpv, criteria = '';

    if (asset.rmpv) {
        return asset.rmpv;
    }

    criteria = asset.printTechnology;

    // if print tech is scan thn send default RMPV for scan
    if (criteria === RmpvConstants.Scan) {
        return RmpvConstants.rmpvMatrix.Scan;
    }

    criteria = criteria + RmpvConstants.deviceTypeMatrix[asset.deviceType];
    // assumend if device is not A3 than it is A4
    if (asset.hasA3) {
        criteria = criteria + RmpvConstants.A3;
    } else {
        criteria = criteria + RmpvConstants.A4;
    }
    rmpv = RmpvConstants.rmpvMatrix[criteria];

    if (!rmpv && (asset.deviceType === Configuration.deviceTypes.plotter || asset.deviceType === Configuration.deviceTypes.fax)) {
        rmpv = RmpvConstants.defaultRmpvForPlotterAndFax;
        return rmpv;
    }

    rmpv = RmpvConstants.rmpvMatrix[criteria] || RmpvConstants.defaultRmpv;

    return rmpv;
}


describe("@getRMPVRequest", () => {

    beforeEach(()=> {
    });

    it('should return rmpv from asset',  () => {
        let Rmpv: number = getRmpv(assets.AssetTestData.assets[0]);
        expect(Rmpv).to.deep.equals(25000);
    });
    it('should return default laserMultifunctionA3 rmpv',  () => {
        let Rmpv: number = getRmpv(assets.AssetTestData.assets[1]);
        expect(Rmpv).to.deep.equals(15000);
    });
    it('should return default laserMultifunctionA4 rmpv',  () => {
        let Rmpv: number = getRmpv(assets.AssetTestData.assets[2]);
        expect(Rmpv).to.deep.equals(7500);
    });
    it('should return default InkMultifunctionA4 rmpv',  () => {
        let Rmpv: number = getRmpv(assets.AssetTestData.assets[3]);
        expect(Rmpv).to.deep.equals(4500);
    });
    it('should return default InkMultifunctionA3 rmpv',  () => {
        let Rmpv: number = getRmpv(assets.AssetTestData.assets[4]);
        expect(Rmpv).to.deep.equals(7500);
    });
    it('should return default inkOtherA3 rmpv',  () => {
        let Rmpv: number = getRmpv(assets.AssetTestData.assets[5]);
        expect(Rmpv).to.deep.equals(3000);
    });
    it('should return default inkOtherA4 rmpv',  () => {
        let Rmpv: number = getRmpv(assets.AssetTestData.assets[5]);
        expect(Rmpv).to.deep.equals(3000);
    });
    it('should return default scan rmpv',  () => {
        let Rmpv: number = getRmpv(assets.AssetTestData.assets[6]);
        expect(Rmpv).to.deep.equals(3000);
    });
    //todo
});